﻿using Foundation;
using UIKit;
using CoreGraphics;
using Pikkart.ArSdk.Geo;

namespace pikkartXamarinFormsDemo.iOS
{
    public class GeoViewMarkerAdapter : PKTMarkerViewAdapter
    {

        public GeoViewMarkerAdapter(CGSize size) : base(size) { }


        public override UIView GetView(PKTGeoElement geoElement)
        {
            return new UIImageView(UIImage.FromBundle("map-marker-Yellow.png"));
        }

        public override UIView GetSelectedView(PKTGeoElement geoElement)
        {
            return new UIImageView(UIImage.FromBundle("map-marker-Orange.png"));
        }
    }

    public class MapViewMarkerAdapter : PKTMarkerViewAdapter
    {

        public MapViewMarkerAdapter(CGSize size) : base(size) { }


        public override UIView GetView(PKTGeoElement geoElement)
        {
            return new UIImageView(UIImage.FromBundle("map-marker-Brown.png"));
        }

        public override UIView GetSelectedView(PKTGeoElement geoElement)
        {
            return new UIImageView(UIImage.FromBundle("map-marker-Black.png"));
        }
    }
}
