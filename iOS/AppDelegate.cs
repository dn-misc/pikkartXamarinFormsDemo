﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using CoreGraphics;
using Pikkart.ArSdk.Geo;

namespace pikkartXamarinFormsDemo.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
       
        public override UIWindow Window
        {
            get;

            set;
        }

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            // Add window

            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            return base.FinishedLaunching(app, options);

        }
    }
}
