﻿using System;
using Foundation;
using UIKit;
using Pikkart.ArSdk.Geo;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Collections.Generic;
using CoreGraphics;
using System.Drawing;
using CoreLocation;

[assembly: ExportRenderer(typeof(pikkartXamarinFormsDemo.PKPage), typeof(pikkartXamarinFormsDemo.iOS.PikkartPageRenderer))]
namespace pikkartXamarinFormsDemo.iOS
{
    public class PikkartPageRenderer : PageRenderer
    {
        PKViewController geoMain;

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {

            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                var page = e.NewElement as PKPage;
                var view = NativeView;

                var hostViewController = ViewController;

                string kGMapManagerGoogleMapsAPIKey = "AIzaSyA7uiTxeRFbbYaK-Y1j6xmXZtcI8ayOb6Y";
                PKTGeoMainController.SetGoogleMapsKey(kGMapManagerGoogleMapsAPIKey);

                Dictionary<string, CLLocation> poiNames = new Dictionary<string, CLLocation>();
                poiNames.Add("Jarun, Zagreb", new CLLocation(45.785019, 15.912709));
                poiNames.Add("Mc Donalds, Zagreb", new CLLocation(45.794924, 15.919747));
                poiNames.Add("City Center One, Zagreb", new CLLocation(45.798874, 15.885329));


                geoMain = new PKViewController(new GeoViewMarkerAdapter(new CGSize(30, 45)),
                                               new MapViewMarkerAdapter(new CGSize(40, 40)), poiNames);

                geoMain.View.Frame = UIScreen.MainScreen.Bounds;

                hostViewController.AddChildViewController(geoMain);
                hostViewController.View.AddSubview(geoMain.View);
                hostViewController.View.BringSubviewToFront(geoMain.View);
                geoMain.View.TranslatesAutoresizingMaskIntoConstraints = false;
                geoMain.View.TopAnchor.ConstraintEqualTo(hostViewController.View.TopAnchor).Active = true;
                geoMain.View.BottomAnchor.ConstraintEqualTo(hostViewController.View.BottomAnchor).Active = true;
                geoMain.View.LeadingAnchor.ConstraintEqualTo(hostViewController.View.LeadingAnchor).Active = true;
                geoMain.View.TrailingAnchor.ConstraintEqualTo(hostViewController.View.TrailingAnchor).Active = true;
                geoMain.DidMoveToParentViewController(hostViewController);



            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"\t\t\tERROR: {ex.Message}");
            }
        }


    }

}
