﻿using System;
using UIKit;
using Pikkart.ArSdk.Geo;
using System.Collections.Generic;
using CoreLocation;

namespace pikkartXamarinFormsDemo.iOS
{
    public class PKViewController : PKTGeoMainController
    {
        Dictionary<string, CLLocation> poiNames;

        public PKViewController() : base() { }

        public PKViewController(PKTMarkerViewAdapter geoAdapter,
                                PKTMarkerViewAdapter mapAdapter, Dictionary<string, CLLocation> poiNamesX) : base(geoAdapter, mapAdapter) {
            poiNames = poiNamesX;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            PKTGeoLocation geoLocation;
            PKTGeoElement geoElement;
            List<PKTGeoElement> geoElements = new List<PKTGeoElement>();

            var index = 0;
            foreach (var pair in poiNames)
            {
                geoLocation = new PKTGeoLocation(pair.Value.Coordinate.Latitude, pair.Value.Coordinate.Longitude, 0);
                geoElement = new PKTGeoElement(geoLocation, index + pair.Key, pair.Key);
                geoElements.Add(geoElement);
                index = index + 1;
            }
            //DisableRecognition();
            EnableGeoAR();
            EnableGeoMap();
            Show(geoElements.ToArray());
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

        }
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.     
        }
    }
}